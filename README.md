This repo is my personal build of the simple (suckless) terminal. The primary features of this build are the Nord colors and scrollback functionality.

st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup. This build of st is installed into the $HOME/.local namespace by default. You may want to change this to /usr/local, which can be done easily through the following command, or by manually editing config.mk and changing the PREFIX variable.

    patch < patches/usr-local.diff

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL \<aurelien dot aptel at gmail dot com\> bt source code.

